#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <Cobs.h>

#define OLED_RESET 4

Adafruit_SSD1306 display(OLED_RESET);

#define LOGO16_GLCD_HEIGHT 16 
#define LOGO16_GLCD_WIDTH  16 

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif




void setup()   {                
  Serial.begin(9600);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  display.cp437();

  // init done
  
  // Show image buffer on the display hardware.
  // Since the buffer is intialized with an Adafruit splashscreen
  // internally, this will display the splashscreen.
  display.display();
  delay(500);

  // Clear the buffer.
  display.clearDisplay();
  display.display();
  
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);

  display.setBinaryMode( BM_MIXT );
}


int g_counter=0;

Cobs cobs( Serial );

// Use arduino loop as main 'event' loop
void loop() {

  if( cobs.read() ) {
    display.setCRMode( CR_DISP );
    // Full RX buffer (62 bytes)
    display.printlen( cobs.getRXBuffer(), 62 );
    display.setCRMode( CR_NORMAL );
    display.print('\n');
    display.setCRMode( CR_DISP );
    // Full RX buffer (62 bytes)
    display.printlen( cobs.getRXBuffer(), cobs.getRXSize() );
    
    display.setCRMode( CR_NORMAL );
    display.print('\n');
    display.print(cobs.getRXStatus());
    display.print('\n');
    display.print(cobs.getRXSize());
    display.print('\n');
    
  }

  display.display(); 
}



