#!/usr/local/bin/python3

import serial
import argparse
import time
import cobs
import re


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input-string", help="String to send through serial", required=True)
    parser.add_argument("-s", "--serial-port", help="Port to send to", required=True)
    return parser.parse_args()


# Function to convert a string to bytestring, where '\xNN' in input, where NN is a 2 digit hexa integer string
# representation) is replaced by the 1 byte sequence b'\xNN'
# The goal is to simply type non-printable characters on the command line and send their raw character code
# to the COM port
def to_byte_array(string):
    bstring = string.encode()
    result=b''

    last_match = 0

    for a_match in re.finditer(b'\\\\x([0-9A-Fa-f]{2})',bstring ):
        print(a_match)
        result += bstring[last_match:a_match.start()]

        result += bytes([int(a_match.group(1), 16)])

        last_match = a_match.end()

    result += bstring[last_match:]

    return result


if __name__ == "__main__":

    args=get_args()

    print(args)

    # Open port with default param: 9600, 8, N, 1
    com = serial.Serial()
    com.port = args.serial_port
    com.baudrate = 9600
    com.timeout = 1
    com.setDTR(True)
    com.open()

    time.sleep(2)

    msg = to_byte_array(args.input_string)
    print(msg)
    frame = cobs.encobs(msg)
    print(frame)
    com.write(frame)


    com.close()



