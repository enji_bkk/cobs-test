# Test for arduino cobs library #

## Arduino sketch

Hardware dependency:

* OLED display 128x64, driver : SSD1306, i2c protocol

Software dependency:

* cobs library (https://bitbucket.org/enji_bkk/cobs)
* fork of Adafruit GFX lib (https://github.com/afaure-bkk/Adafruit-GFX-Library)
* fork of Adafruit SSD1306 lib (https://github.com/afaure-bkk/Adafruit_SSD1306)

## Python test program

External modules:
* Serial communication: pySerial (https://pythonhosted.org/pyserial/)

Usage:
python3 serialframe.py -i 'Potatoes!\x05' -s /dev/ttyACM0
